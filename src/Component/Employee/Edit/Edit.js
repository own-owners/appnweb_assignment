import React, { Component } from 'react'
import './Edit.css';

export class Edit extends Component {

    state = {
        empName: "",
        empSalary: "",
        empAge: "",
    }

    handlerStateChange = (type) => (event) => {
        this.setState({
            [type]: event.target.value,
        });
    }

    handlerSubmit = (e) => {
        e.preventDefault()
        console.log(this.state);
    }
    render() {
        return (
            <div>
                <form onSubmit={this.handlerSubmit}>
                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <label>Employee Name</label>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Employee Name"
                                value={this.state.empName}
                                onChange={this.handlerStateChange("empName")}
                            />
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <label>Employee Salary</label>
                            <input type="number" name="empSalary" className="form-control" id="inputSalary" placeholder="Employee Salary" value={this.state.empSalary}
                            onChange={this.handlerStateChange("empName")} />
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <label >Employee Age</label>
                            <input type="number" name="empAge" className="form-control" id="inputAge" placeholder="Employee Age" value={this.state.empAge} 
                            onChange={this.handlerStateChange("empName")}/>
                        </div>
                    </div>
                    <button type="submit" className="btn btn-primary">Sign in</button>
                </form>
            </div>
        )
    }
}

export default Edit
