import React from 'react'

import { Switch, Route } from 'react-router-dom';
import List from './List/List';
import Edit from './Edit/Edit';


const route = (props) => {
    console.log(props.match.path);
    return (
        <Switch>
            <Route exact path={props.match.path} component={List} />
            <Route exact path={[`${props.match.path}/new`]} component={Edit} />
        </Switch>
    );
}

export default route
