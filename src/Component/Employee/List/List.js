import React, { Component } from 'react'
import './List.css';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import * as Icons from "@fortawesome/free-solid-svg-icons";
import axios from '../../../axios';
import Edit from '../Edit/Edit';
import { Link } from 'react-router-dom'; 

class List extends Component {
    state = {
        employeeList: null
    }

    /**
     * Get data list
     */
    getUserList = () => {
        axios.get("/employees")
            .then(response => {
                this.setState({ employeeList: response.data });
                console.log(response);
            })
            .catch(error => {
                this.setState({ error: error.data })
            });
    }

    componentDidMount() {
        this.getUserList();
    }
    render() {
        return (
            <div className="container bootstrap snippet">
                <div className="row">
                    <button className='btn btn-denger' style={{}}>  <Link
                        to={"/employee/new"}
                        style={{ textDecoration: 'none' }}
                    >{"Add Employee"}
                    </Link> </button>
                    <div className="col-lg-12">
                        <div className="main-box no-header clearfix">
                            <div className="main-box-body clearfix">
                                <div className="table-responsive">
                                    <table className="table user-list" style={{ overflow: "hidden" }}>
                                        <thead>
                                            <tr>
                                                <th><span>Name</span></th>
                                                <th><span>Salary</span></th>
                                                <th className="text-center"><span>Age</span></th>
                                                <th>&nbsp;</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {(Array.isArray(this.state.employeeList))
                                                ?
                                                this.state.employeeList.map(data => {
                                                    return (
                                                        <tr key={data.id}>
                                                            <td>
                                                                <img src={data.profile_image || "https://bootdey.com/img/Content/user_1.jpg"} alt="" />
                                                                <a href="#" className="user-link">{(data.employee_name) ? data.employee_name : null}</a>
                                                            </td>
                                                            <td>{(data.employee_salary) ? data.employee_salary : null}</td>
                                                            <td className="text-center">
                                                                <span>{(data.employee_age) ? data.employee_age : null}</span>
                                                            </td>
                                                            <td>
                                                                <div className="row">
                                                                    <button className='btn btn-primary' style={{ width: "30%" }}>
                                                                        <FontAwesomeIcon icon={Icons.faEdit} />
                                                                    </button>
                                                                    <button className='btn btn-danger ' style={{ width: "30%" }}>
                                                                        <FontAwesomeIcon icon={Icons.faTimes} />
                                                                    </button>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    );
                                                })
                                                :
                                                null}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default List
