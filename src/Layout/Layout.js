import React from 'react'

import Header from '../Layout/Header/Header';
import Footer from '../Layout/Footer/Footer';

const Layout = (props) => {
    return (
        <React.Fragment>
            <Header />
            {props.children}
            <Footer />
        </React.Fragment>
    )
}

export default Layout
