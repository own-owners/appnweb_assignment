import React from 'react'

// import { createBrowserHistory } from "history";
import Router from './Router/Router';

/**
 * create history object 
 */
// const history = createBrowserHistory();

const App = (props) => {
  
  /**
   * Check User authentication
   */
  
  // if (!validUser) {
  //   history.push("/login")
  // }
  

  return (
    <Router />
  )
}

export default App
