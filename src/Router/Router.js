import React from 'react'

import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import Login from '../Component/Core/login/login';
import Layout from '../Layout/Layout';
import Home from '../Component/Home/Home';
// import Notfound from '../Component/Core/Notfound/Notfound';
import Employee from '../Component/Employee';
import List from '../Component/Employee/List/List';
import Edit from '../Component/Employee/Edit/Edit';

/**
 * get users list from json file
 */
import users from '../Component/Core/Users.json';
const userList = users.userList;

/**
 * User authentication
 * 
 * return true/false
 */
const checkValidUser = () => {
    const token = localStorage.getItem("token");
    let valid = false;
    userList.map(user => {
        if (user.token === token) {
            valid = true
        }
        return true;
    });
    return token
}


const router = (props) => {
    const validUser = checkValidUser();
    console.log(validUser);
    return (
        <Router>
            <Route path="/login" exact component={Login} />
           { (validUser)
                ?
            <Layout>
                <Route exact path={[`/employee/new`, `/employee/edit/:id`]} component={Edit} />
                <Route path="/employee" exact component={List} />
                <Route path="/" exact component={Home} />
            </Layout>
            :
            <Redirect from="" to={"/login"} />}

            {/* {
            } */}
            {/* <Route component={Notfound} /> */}
        </Router>
    )
}

export default router;