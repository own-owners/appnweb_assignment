import React from 'react';
import './Employee.css';
import { Link } from 'react-router-dom';


const employee = ()=>{
    return(
        <div className="row" style={{margin:'15px'}}>
            <div className="col-sm-6">
                <div className="card">
                    <div className="card-body">
                        <h5 className="card-title">Employee</h5>
                        <p className="card-text">With supporting text below as a natural lead-in to additional content.</p>
                        <a href="#" className="btn btn-primary">Go somewhere</a>
                        <Link
                            to={"/employeelist"}
                            style={{ textDecoration: 'none' }}
                        >{" Employee List"}
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default employee;