import React from 'react';
import './EmployeeList.css';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import * as Icons from "@fortawesome/free-solid-svg-icons";

const employeeList = (props) => {
    console.log(props);
    return (
        <div className="container bootstrap snippet">
            <div className="row">
                <div className="col-lg-12">
                    <div className="main-box no-header clearfix">
                        <div className="main-box-body clearfix">
                            <div className="table-responsive">
                                <table className="table user-list" style={{ overflow: "hidden" }}>
                                    <thead>
                                        <tr>
                                            <th><span>Name</span></th>
                                            <th><span>Salary</span></th>
                                            <th className="text-center"><span>Age</span></th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {(Array.isArray(props.employeeList))
                                            ?
                                            props.employeeList.map(data => {
                                                return (
                                                    <tr key={data.id}>
                                                        <td>
                                                            <img src={data.profile_image || "https://bootdey.com/img/Content/user_1.jpg"} alt="" />
                                                            <a href="#" className="user-link">{(data.employee_name) ? data.employee_name : null}</a>
                                                        </td>
                                                        <td>{(data.employee_salary) ? data.employee_salary : null}</td>
                                                        <td className="text-center">
                                                            <span>{(data.employee_age) ? data.employee_age : null}</span>
                                                        </td>
                                                        <td>
                                                            <div className="row">
                                                                <div style={{ width: "30%" }}>
                                                                    <FontAwesomeIcon icon={Icons.faEdit} />
                                                                </div>
                                                                <div style={{ width: "30%" }}>
                                                                    <FontAwesomeIcon icon={Icons.faTimes} />
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                );
                                            })
                                            :
                                            null}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

}
export default employeeList;